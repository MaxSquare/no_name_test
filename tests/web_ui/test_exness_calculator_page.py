from web_pages.exness_calculator_page import ExnessCalculatorPage
from utils.logger import log
from services.http.rest_service.calculator_service import CalculatorServiceClient


def test_margin_calculation(web_driver):
    calc_http = CalculatorServiceClient(validate=True)
    res = calc_http.get_calculation(form_type="classic", lot="10", leverage="200",
                                    instrument="Forex", symbol="AUDCAD", user_currency="USD")
    log.info(res.json)
    margin_formula_api = res.json['margin_formula2']
    log.info(margin_formula_api)

    calculator_page = ExnessCalculatorPage(web_driver)
    calculator_page.open_calculator_page()
    margin_formula_web = calculator_page.calculate()
    log.info(margin_formula_web)
    assert margin_formula_api == '{0} x {1} / {2} = {3}'.format(margin_formula_web[0], margin_formula_web[1],
                                                                margin_formula_web[2], margin_formula_web[3])

    # pipenv run pytest tests/web_ui/test_exness_calculator_page.py
