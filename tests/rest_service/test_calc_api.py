import pytest
from services.http.rest_service.calculator_service import CalculatorServiceClient
from utils.logger import log
from test_data import POPULAR_CURRENCY
from test_data import LEVERAGE_BOUNDARIES
from test_data import LOT_BOUNDARIES
from test_data import UNSUPPORTED_CURRENCY


def test_get_calculated():
    calc_http = CalculatorServiceClient(validate=True)
    res = calc_http.get_calculation(form_type="mini", lot="0.21", leverage="2", instrument="Forex",
                                    symbol="AUDCADm", user_currency="USD")
    log.info(res.json)
    assert res.status == 200


@pytest.mark.parametrize("symbols", POPULAR_CURRENCY)
def test_get_calculate_all_instruments(symbols):
    calc_http = CalculatorServiceClient(validate=True)
    res = calc_http.get_calculation(form_type="classic", lot="0.21", leverage="2",
                                    instrument="Forex", symbol=str(symbols[0]), user_currency="USD")
    log.info(res.json)
    assert res.status == 200


@pytest.mark.parametrize("leverage_boundaries", LEVERAGE_BOUNDARIES)
def test_get_calculate_leverage_boundaries(leverage_boundaries):
    calc_http = CalculatorServiceClient(validate=True)
    res = calc_http.get_calculation(form_type="classic", lot="0.21", leverage=int(leverage_boundaries[0]),
                                    instrument="Forex", symbol="AUDCAD", user_currency="USD")
    log.info(res.json)
    assert res.json['leverage'][0] == '\"{}\" is not a valid choice.'.format(leverage_boundaries[0])


@pytest.mark.parametrize("lot_boundaries", LOT_BOUNDARIES)
def test_get_calculate_lot_boundaries(lot_boundaries):
    calc_http = CalculatorServiceClient(validate=True)
    res = calc_http.get_calculation(form_type="classic", lot=lot_boundaries[0], leverage="2", instrument="Forex",
                                    symbol="AUDCAD", user_currency="USD")
    log.info(res.json)
    assert res.json['lot'][0] == "Ensure this value is greater than or equal to 0.01."


@pytest.mark.parametrize("unsupported_currency", UNSUPPORTED_CURRENCY)
def test_get_calculate_unsupported_currency(unsupported_currency):
    calc_http = CalculatorServiceClient(validate=True)
    res = calc_http.get_calculation(form_type="classic", lot="0.21", leverage="2", instrument="Forex",
                                    symbol="AUDCAD", user_currency=unsupported_currency[0])
    log.info(res.json)
    assert res.json['user_currency'][0] == '\"{}\" is not a valid choice.'.format(unsupported_currency[0])

    # pipenv run pytest tests/rest_service/test_calc_api.py
