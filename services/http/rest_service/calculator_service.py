from services.http.rest_service.base import RestService
from utils.enums.endpoints import Endpoints


class CalculatorServiceClient(RestService):

    def get_calculation(self, form_type, lot, leverage, instrument, symbol, user_currency):
        req = self.get()
        req.uri(Endpoints.calculator.value)
        req.query_params(**{"form_type": form_type, "lot": lot, "leverage": leverage, "instrument": instrument,
                         "symbol": symbol, "user_currency": user_currency})
        return req.send()
