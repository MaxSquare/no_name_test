from utils.read_csv import read_from_csv


_currency = 'test_data/currency.csv'
_format_type = 'test_data/format_type.csv'
_symbol = 'test_data/symbol.csv'
_leverage = 'test_data/leverage.csv'
_leverage_boundaries = 'test_data/leverage_boundary_values.csv'
_lot_boundaries = 'test_data/lot_boundaries.csv'
_unsupported_currency = 'test_data/unsupported_currency.csv'
_popular_currency = 'test_data/popular_currency_pairs.csv'

CURRENCY = read_from_csv(_currency)
FORMAT_TYPE = read_from_csv(_format_type)
SYMBOL = read_from_csv(_symbol)
LEVERAGE = read_from_csv(_leverage)
LEVERAGE_BOUNDARIES = read_from_csv(_leverage_boundaries)
LOT_BOUNDARIES = read_from_csv(_lot_boundaries)
UNSUPPORTED_CURRENCY = read_from_csv(_unsupported_currency)
POPULAR_CURRENCY = read_from_csv(_popular_currency)
