from web_pages.base_page import BasePage
from configs import WEB_URL
from utils.enums.urls import Urls
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys


class ExnessCalculatorPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def open_calculator_page(self):
        self.driver.get(WEB_URL + Urls.calculator_url.value)
        self.wait_page_loaded()

    def calculate(self, form_type="Pro", lot="10", leverage="1:200", instrument="AUDCAD", user_currency="USD"):
        form_type_select =\
            Select(self.wait.until_element_visible_by_xpath("//div[@class='inp-group__case'][1]//div" + 
                                                            "[@class='select__box']/select[@name='account_type']"))
        form_type_select.select_by_value(f'{form_type}')

        instrument_select =\
            Select(self.wait.until_element_visible_by_xpath("//div[@class='inp-group__case'][2]//div" + 
                                                            "[@class='select__box']/select[@name='forex']"))
        instrument_select.select_by_value(f'{instrument}')

        lot_input =\
            self.wait.until_element_visible_by_xpath("//div[@class='inp-group__case'][3]//div" + 
                                                     "[@class='inp__box']/input[@name='lot']")
        lot_input.send_keys(Keys.CONTROL, "a")
        lot_input.send_keys(lot)

        leverage_select =\
            Select(self.wait.until_element_visible_by_xpath("//div[@class='inp-group__case'][4]//div" + 
                                                            "[@class='select__box']/select[@name='leverage']"))
        leverage_select.select_by_value(f'{leverage}')

        user_currency_select =\
            Select(self.wait.until_element_visible_by_xpath("//div[@class='inp-group__case'][5]//div" + 
                                                            "[@class='select__box']/select[@name='account_currency']"))
        user_currency_select.select_by_value(f'{user_currency}')

        calculate_btn = self.wait.until_element_visible_by_xpath("//div[@class='inp-group__case'][6]/button")
        calculate_btn.click()
        self.wait_page_loaded()

        margin_formula_list_values = []
        margin_formula_lot =\
            self.wait.until_element_visible_by_xpath("//div[@class='formula'][1]//div[@class='formula__item']/div/p")
        margin_formula_lot_value = margin_formula_lot.text
        margin_formula_list_values.append(margin_formula_lot_value)

        margin_formula_contract_size =\
            self.wait.until_element_visible_by_xpath("//div[@class='formula'][1]//div" + 
                                                     "[@class='formula__item']/div[3]/p")
        margin_formula_contract_size_value = margin_formula_contract_size.text
        margin_formula_list_values.append(margin_formula_contract_size_value)

        margin_formula_leverage = self.wait.until_element_visible_by_xpath("//div[@class='formula'][1]//div" + 
                                                                           "[@class='formula__item']/div[5]/p")
        margin_formula_leverage_value = margin_formula_leverage.text
        margin_formula_list_values.append(margin_formula_leverage_value)

        margin_formula_result = self.wait.until_element_visible_by_xpath("//div[@class='formula'][1]//div" + 
                                                                         "[@class='formula__item']/div[7]/p")
        margin_formula_result_value = margin_formula_result.text
        margin_formula_list_values.append(margin_formula_result_value)

        return margin_formula_list_values
