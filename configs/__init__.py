import os
from os import getenv
from utils.logger import log

from dotenv import load_dotenv

from configs.env_config import get_env_config
from utils import PROJECT_PATH

dotenv_file = os.path.join(PROJECT_PATH, ".env")
if os.path.exists(dotenv_file):
    load_dotenv(dotenv_file)

# Test environment configuration
ENV = getenv("ENV")
_env_config = get_env_config(ENV)

log.info(f"Environment is: {ENV}")

# API hosts
REST_SERVICE_HOST = _env_config["host"]

# UI wait time
UI_MAX_RESPONSE_TIME = 25.0

# Webdriver
WEB_URL = _env_config["host"]
WEB_DRIVER = "CHROME"
HEADLESS = False
