## Setup, install dependencies.

Here, pipenv is used instead of pip. Run in command line:

1. Install Python 3.7.0
2. Run in command line: `pip install pipenv`
3. Run in command line: `pipenv install` (if the virtualenv is already activated, you can also use `pipenv sync`). Consider `pipenv install --dev` and `pipenv sync --dev` in case of dev dependencies.

Why pipenv?

Putting the dependencies into a requirements.txt and then using pip will work but is not really necessary. The whole point of using pipenv for most people is to avoid the need to manage a requirements.txt or to use pip.

---

## Run code static analysis.

- Run flake8: `pipenv run flake8 .`

---

## Run API tests.

- Create ".env" file with "ENV=development" row in it in the root directory.
- Read commented row in file with tests (run `pipenv run pytest tests/rest_service/test_calc_api.py`).
- Run all tests: `pipenv run pytest`
- Run project related tests: `pipenv run pytest tests/{project_name}`
- Run project related tests subset: `pipenv run pytest tests/{project_name}/{tests_file}.py`
- Run project related test: `pipenv run pytest tests/{project_name}/{tests_file}.py::{test_name}`

---

## To run web UI tests.

- You should have driver placed in the root directory.
- Set its corresponding name in config, `WEB_DRIVER` variable, set to CHROME now.
- In config, `HEADLESS` is set to False now.
- Read commented row in file with tests (run `pipenv run pytest tests/web_ui/test_exness_calculator_page.py`).
