from enum import Enum


class Endpoints(Enum):
    """
    All kind of API endpoints for different services
    """

    # Users service
    calculator = "/api/calculator/calculate"
