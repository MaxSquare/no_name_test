from enum import Enum


class Urls(Enum):
    """
    All kind of web urls for test to concatenate with base url
    """

    calculator_url = "/calculator"
